package singleton;

public class Main {
    public static void main(String[] args){
        Singleton first = Singleton.getSingleton();
        Singleton second = Singleton.getSingleton();
        Singleton third = Singleton.getSingleton();

        System.out.println("I will draw first picture " + first.action);
        System.out.println("I will draw second picture " + second.action);
        System.out.println("I will draw third picture " + third.action);
    }
}
