package singleton;

public class Singleton {
    public static Singleton INSTANCE=null;
    public String action;
    private Singleton(){
        action = "with this red pen.\n";
    }

    public static Singleton getSingleton(){
        if(INSTANCE == null){
            INSTANCE = new Singleton();
        }
        return INSTANCE;
    }
}
